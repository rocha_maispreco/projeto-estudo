﻿namespace MSDPortal.Domain.Enum
{
    public enum ProdutoEnum
    {
        Admin = 1,
        Distributor = 2,
        Seller = 3,
        PetShop = 4
    }
}
