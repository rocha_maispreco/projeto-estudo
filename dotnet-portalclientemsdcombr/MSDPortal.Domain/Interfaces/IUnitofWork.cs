﻿using MSDPortal.Domain.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSDPortal.Domain.Interfaces
{
    public interface IUnitofWork : IDisposable
    {
        void Dispose(bool disposing);
        IProdutoRepository ProdutoRepository { get; }
        void Save();
    }
}
