﻿using MSDPortal.Application.Entidades;

namespace MSDPortal.Domain.Interfaces.Repository
{
    public interface IProdutoRepository : IBaseRepository<Produto>
    {

    }
}
