﻿using MSDPortal.Domain.Entidades;

namespace MSDPortal.Application.Entidades
{
    public class Produto : BaseEntity
    {
        public string Nome { get; set; }
    }
}
