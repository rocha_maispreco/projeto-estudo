﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using MSDPortal.Presentation.Web.Models;
using Owin;

[assembly: OwinStartup(typeof(MSDPortal.Presentation.Web.Startup))]
namespace MSDPortal.Presentation.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            CreateRolesandUsers();
        }

        // In this method we will create default User roles and Admin user for login    
        private void CreateRolesandUsers()
        {
            //ApplicationDbContext context = new ApplicationDbContext();

            //var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            //var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));


            //if (!roleManager.RoleExists("Admin"))
            //{
            //    var role = new IdentityRole();
            //    role.Name = "Admin";
            //    roleManager.Create(role);
            //}

            ////TODO: Colocar no Web.config
            //var user = new ApplicationUser();
            //user.UserName = "MSDAdmin";
            //user.Email = "dev01@housecricket.com.br";
            //string userPWD = "msdadmin123";

            //var resultAdmin = UserManager.FindByEmail(user.Email);

            //if (resultAdmin == null)
            //{
            //    UserManager.Create(user, userPWD);
            //    UserManager.AddToRole(user.Id, "Admin");
            //}


            //if (!roleManager.RoleExists("PetShop"))
            //{
            //    var role = new IdentityRole();
            //    role.Name = "PetShop";
            //    roleManager.Create(role);
            //}

            //if (!roleManager.RoleExists("PetShopConvidado"))
            //{
            //    var role = new IdentityRole();
            //    role.Name = "PetShopConvidado";
            //    roleManager.Create(role);

            //}

            //// creating Creating Employee role     
            //if (!roleManager.RoleExists("Distribuidor"))
            //{
            //    var role = new IdentityRole();
            //    role.Name = "Distribuidor";
            //    roleManager.Create(role);

            //}

            //if (!roleManager.RoleExists("Vendedor"))
            //{
            //    var role = new IdentityRole();
            //    role.Name = "Vendedor";
            //    roleManager.Create(role);

            //}
        }
    }
}
