﻿using MSDPortal.Application;
using MSDPortal.Application.Entidades;
using MSDPortal.Domain.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MSDPortal.Presentation.Web.Controllers
{
    public class ProdutoController : Controller
    {
        private readonly ProdutoService _produtoService;
        
        public ProdutoController(ProdutoService produtoService)
        {
            _produtoService = produtoService;
        }
        
        // GET: Produto
        public async Task<ActionResult> Index()
        {
          List<Produto> listaProdutos = await _produtoService.GetAll();
          return View(listaProdutos);
        }

        // GET: Produto/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Produto/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Produto/Create
        [HttpPost]
        public async Task<ActionResult> Create(Produto model)
        {
            try
            {
               await _produtoService.Add(model);
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Produto/Edit/5
        public async Task<ActionResult> Edit(int id)
        {

            Produto produto = await _produtoService.Get(id);
            return View(produto);
        }

        // POST: Produto/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(int id, Produto model)
        {
            try
            {
                await _produtoService.Update(model);

                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Produto/Delete/5
        public ActionResult Delete(int id)
        {

            return View(new Produto());
        }

        // POST: Produto/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(int id, Produto model)
        {
            try
            {
                // TODO: Add delete logic here
                _produtoService.Delete(model);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
