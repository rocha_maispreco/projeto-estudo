﻿using MSDPortal.Domain.Enum;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MSDPortal.Presentation.Web.Modelsdsadsa
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required(ErrorMessage = "{0} é obrigatório", AllowEmptyStrings = false)]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required(ErrorMessage = "{0} é obrigatório", AllowEmptyStrings = false)]
        public string Provider { get; set; }

        [Required(ErrorMessage = "{0} é obrigatório", AllowEmptyStrings = false)]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required(ErrorMessage = "{0} é obrigatório", AllowEmptyStrings = false)]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required(ErrorMessage = "{0} é obrigatório", AllowEmptyStrings = false)]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "O campo Email não contém um endereço de email válido")]
        public string Email { get; set; }

        [Required(ErrorMessage = "{0} é obrigatório", AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required(ErrorMessage = "{0} é obrigatório", AllowEmptyStrings = false)]
        [EmailAddress(ErrorMessage = "O campo Email não contém um endereço de email válido")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "{0} é obrigatório", AllowEmptyStrings = false)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        //[Display(Name = "Perfil")]
        //public PerfilEnum Perfil { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required(ErrorMessage = "{0} é obrigatório", AllowEmptyStrings = false)]
        [EmailAddress(ErrorMessage = "O campo Email não contém um endereço de email válido")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "{0} é obrigatório", AllowEmptyStrings = false)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage = "{0} é obrigatório", AllowEmptyStrings = false)]
        [EmailAddress(ErrorMessage = "O campo Email não contém um endereço de email válido")]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
