﻿using MSDPortal.Data.Context;
using MSDPortal.Domain.Entidades;
using MSDPortal.Domain.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace MSDPortal.Data.Repository
{
    public class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        internal MSDPortalContext _context;

        public BaseRepository(MSDPortalContext context)
        {
            _context = context;
        }

        public async Task<T> Add(T obj)
        {
            obj.DataCriacao = DateTime.Now;
            _context.Set<T>().Add(obj);

            await _context.Salvar();
       //     await _context.SaveChangesAsync();

            return obj;
        }

        public virtual IEnumerable<T> GetAll()
        {
            var result = new List<T>();
            try
            {
                result = _context.Set<T>().ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var result = new List<T>();
            try
            {
                result = await _context.Set<T>().ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public async Task<T> GetById(int id)
        {
            return await _context.Set<T>().FindAsync(id);
        }

        public void Remove(T obj)
        {
            _context.Entry(obj).State = EntityState.Deleted;
            _context.Set<T>().Remove(obj);
            _context.SaveChanges();
        }

        public async Task<T> Update(T obj)
        {
            obj.DataCriacao = DateTime.Now;

            _context.Entry(obj).State = EntityState.Modified;
            var contexto = _context.Set<T>().Find(obj.Id);
            
            if (!contexto.Equals(obj))
            {
                _context.Set<T>().Remove(contexto);
            }
            await _context.Salvar();

            //var resultado = await _context.SaveChangesAsync();

            return obj;

        }
    }
}
