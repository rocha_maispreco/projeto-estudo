﻿using MSDPortal.Application.Entidades;
using MSDPortal.Domain.Entidades;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace MSDPortal.Data.Context
{
    public class MSDPortalContext : DbContext
    {

        public MSDPortalContext() : base("DefaultConnection")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<MSDPortalContext, Migrations.Configuration>());
            Database.SetInitializer<MSDPortalContext>(new MigrateDatabaseToLatestVersion<MSDPortalContext, MSDPortal.Data.Migrations.Configuration>());

        }
        public DbSet<Produto> Produto { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Produto>().ToTable("Produto");
        }

        public async Task<int> Salvar()
        {
            var entries = ChangeTracker
                .Entries()
                .Where(e => e.Entity is BaseEntity && (
                        e.State == EntityState.Added
                        || e.State == EntityState.Modified));

            foreach (var entityEntry in entries)
            {
                ((BaseEntity)entityEntry.Entity).DataAtualizacao = DateTime.Now;

                if (entityEntry.State == EntityState.Added)
                {
                    ((BaseEntity)entityEntry.Entity).DataCriacao = DateTime.Now;
                }
            }

            return await base.SaveChangesAsync();
        }
    }
}

