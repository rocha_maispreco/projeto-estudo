﻿namespace MSDPortal.Data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MSDPortal.Data.Context.MSDPortalContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "MSDPortal.Data.Context.MSDPortalContext";
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(MSDPortal.Data.Context.MSDPortalContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
        }
    }
}
